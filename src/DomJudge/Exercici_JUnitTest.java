package DomJudge;

import static org.junit.Assert.*;

import org.junit.Test;

public class Exercici_JUnitTest {

	@Test
	public final void testNivellsExp() {
		assertEquals("0 20", Exercici_JUnit.calcularNivell(1));
		assertEquals("0 40", Exercici_JUnit.calcularNivell(2));
		assertEquals("1 0", Exercici_JUnit.calcularNivell(5));
		assertEquals("2 17", Exercici_JUnit.calcularNivell(10));
		assertEquals("4 32", Exercici_JUnit.calcularNivell(18));
		assertEquals("4 120", Exercici_JUnit.calcularNivell(20));
		assertEquals("9 106", Exercici_JUnit.calcularNivell(35));
		assertEquals("14 9", Exercici_JUnit.calcularNivell(48));
		assertEquals("42 453", Exercici_JUnit.calcularNivell(120));

	}

}
