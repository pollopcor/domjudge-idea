package DomJudge;

import java.util.Scanner;

public class Exercici_JUnit {

	public static void main(String[] args) {

		Scanner scn = new Scanner(System.in);

		int casos = scn.nextInt();
		
		for(int i=0;i<casos;i++)
			System.out.println(calcularNivell(scn.nextInt()));

		scn.close();
	}

	public static String calcularNivell(int enemics_derrotats) {

		int exp_personaje = 0; // Experiencia inicial personatge
		int nivell = 0; // Tornar a 0
		int exp_total_nivell = 100; // Experiencia total del nivell
		int exp_per_monstre = 20; // Experiencia guanyada per monstre derrotat

		// Per cada enemic derrotat
		for (int i = 0; i < enemics_derrotats; i++) {

			// Calcula la exp guanyada
			exp_personaje += exp_per_monstre;

			// Si te suficient per pasar de nivell
			if (exp_personaje >= exp_total_nivell) {

				exp_personaje -= exp_total_nivell; // Posar la experiencia sobrant al nou nivell

				nivell++; // Suma un nivell

				// Incrementa la experiencia necessaria per el seguent nivell (+15%)
				exp_total_nivell += 13;
				// Incrementa l'experiencia que et donaran els seguents monstres
				exp_per_monstre += 6;

			}
		}

		return nivell + " " + exp_personaje;

	}

}
