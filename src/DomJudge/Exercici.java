package DomJudge;

import java.util.Scanner;

/**
 * @author <a href="mailto:pollopcor@gmail.com">Pol Lopez</a>
 * @version 1.0
 * @since 1.0
 */

public class Exercici {

	public static void main(String[] args) {

		Scanner scn = new Scanner(System.in);
		int casos;

		casos = scn.nextInt();

		/**
		 * Llegeix els casos de prova. Per cada cas de prova, calcula.
		 */

		for (int c = 0; c < casos; c++) {

			// Llegeix el numero d'enemic derrotats
			int enemics_derrotats = scn.nextInt();

			int exp_personaje = 0; // Experiencia inicial personatge
			int nivell = 0; // Tornar a 0
			int exp_total_nivell = 100; // Experiencia total del nivell
			int exp_per_monstre = 20; // Experiencia guanyada per monstre derrotat

			/**
			 * Per cada enemic que ha derrotat: - Calcula la experiencia guanyada
			 */

			for (int i = 0; i < enemics_derrotats; i++) {

				exp_personaje += exp_per_monstre;

				/**
				 * Si te suficient experiencia, pujara de nivell:
				 * 
				 * <b>Pujar de nivell:</b> - - Treure la experiencia d'aquell nivell - Sumar un
				 * nivell - Incrementar la experiencia necessaria per el seguent nivell (+13) -
				 * Incrementar la experiencia que et donaran els seguents monstres (+6)
				 */
				if (exp_personaje >= exp_total_nivell) {

					exp_personaje -= exp_total_nivell;

					nivell++;

					exp_total_nivell += 13;
					exp_per_monstre += 6;
				}

			}

			/**
			 * Mostrara el nivell y la experiencia que te en aquell nivell
			 */

			System.out.println(nivell + " " + exp_personaje);

		}

		scn.close();
	}

}
